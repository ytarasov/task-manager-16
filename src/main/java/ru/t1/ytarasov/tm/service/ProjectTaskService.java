package ru.t1.ytarasov.tm.service;

import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.service.IProjectTaskService;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ytarasov.tm.exception.field.TaskIdEmptyException;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String taskId, final String projectId) throws AbstractException {
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(final String projectId) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        List<Task> tasksToRemove = taskRepository.findAllTasksByProjectId(project.getId());
        for (Task task : tasksToRemove) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String taskId, final String projectId) throws AbstractException {
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        final Task task = taskRepository.findOneById(taskId);
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

}
