package ru.t1.ytarasov.tm.component;

import ru.t1.ytarasov.tm.api.controller.ICommandController;
import ru.t1.ytarasov.tm.api.controller.IProjectController;
import ru.t1.ytarasov.tm.api.controller.IProjectTaskController;
import ru.t1.ytarasov.tm.api.controller.ITaskController;
import ru.t1.ytarasov.tm.api.repository.ICommandRepository;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.service.*;
import ru.t1.ytarasov.tm.constant.ArgumentConst;
import ru.t1.ytarasov.tm.constant.TerminalConst;
import ru.t1.ytarasov.tm.controller.CommandController;
import ru.t1.ytarasov.tm.controller.ProjectController;
import ru.t1.ytarasov.tm.controller.ProjectTaskController;
import ru.t1.ytarasov.tm.controller.TaskController;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.ytarasov.tm.exception.system.CommandNotSupportedException;
import ru.t1.ytarasov.tm.repository.CommandRepository;
import ru.t1.ytarasov.tm.repository.ProjectRepository;
import ru.t1.ytarasov.tm.repository.TaskRepository;
import ru.t1.ytarasov.tm.service.*;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class Bootstrap {
;
    private final ICommandRepository commandRepository = new CommandRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final ILoggerService loggerService = new LoggerService();

    public void runArguments(String[] args) {
        if (args == null || args.length == 0) {
            runCommands();
            return;
        }
        final String arg = args[0];
        try {
            runArgument(arg);
            loggerService.command(arg);
        } catch (final AbstractException e) {
            loggerService.error(e);
            System.out.println();
        }
    }

    private void runArgument(final String arg) throws AbstractException {
        commandController.displayWelcome();
        if (arg == null || arg.isEmpty()) throw new ArgumentNotSupportedException();
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
    }

    private void runCommands() {
        initLogger();
        System.out.println();
        initDemoData();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                String command = TerminalUtil.nextLine();
                runCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println();
                System.out.println("[FAIL]");
            }
        }
    }

    private void runCommand(final String command) throws AbstractException {
        if (command == null || command.isEmpty()) throw new CommandNotSupportedException();
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.INFO:
                commandController.displayInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeById();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.PROJECT_CHANGE_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTask();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.TASK_CHANGE_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_CHANGE_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.BIND_TASK_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConst.UNBIND_TASK_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case TerminalConst.SHOW_TASKS_BY_PROJECT_ID:
                taskController.findTasksByProjectId();
                break;
            case TerminalConst.EXIT:
                exitApplication();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    private void initDemoData() {
        try {
            projectService.create("BETA PROJECT", "This is beta project", Status.IN_PROGRESS);
            projectService.create("ALPHABET", "This is alphabet project", Status.COMPLETED);
            projectService.create("ANT", "This is ant project", Status.NOT_STARTED);
            taskService.create("MAIN TASK", "This is main task", Status.IN_PROGRESS);
            taskService.create("ZED TASK", "This is zed task", Status.NOT_STARTED);
            taskService.create("SECONDARY TASK", "This is secondary task", Status.COMPLETED);
        } catch (AbstractException e) {
            loggerService.error(e);
            System.out.println();
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO THE TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    public void exitApplication() {
        System.exit(0);
    }

}
