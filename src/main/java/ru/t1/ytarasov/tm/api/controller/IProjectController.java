package ru.t1.ytarasov.tm.api.controller;

import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.Project;

public interface IProjectController {

    void createProject() throws AbstractException;

    void clearProjects();

    void showProjects();

    void showProject(Project project) throws AbstractException;

    void showProjectById() throws AbstractException;

    void showProjectByIndex() throws AbstractException;

    void updateProjectById() throws AbstractException;

    void updateProjectByIndex() throws AbstractException;

    void startProjectById() throws AbstractException;

    void startProjectByIndex() throws AbstractException;

    void completeProjectById() throws AbstractException;

    void completeProjectByIndex() throws AbstractException;

    void changeProjectStatusById() throws AbstractException;

    void changeProjectStatusByIndex() throws AbstractException;

    void removeById() throws AbstractException;

    void removeByIndex() throws AbstractException;

}
