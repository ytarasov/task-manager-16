package ru.t1.ytarasov.tm.api.controller;

import ru.t1.ytarasov.tm.exception.AbstractException;

public interface IProjectTaskController {

    void bindTaskToProject() throws AbstractException;

    void unbindTaskFromProject() throws AbstractException;

}
