package ru.t1.ytarasov.tm.api.service;

import ru.t1.ytarasov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
