package ru.t1.ytarasov.tm.controller;

import ru.t1.ytarasov.tm.api.controller.ICommandController;
import ru.t1.ytarasov.tm.api.service.ICommandService;
import ru.t1.ytarasov.tm.model.Command;
import ru.t1.ytarasov.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayHelp() {
        System.out.println("[HELP]");
        Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) System.out.println(command);
        System.out.println();
    }

    @Override
    public void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.16.2");
        System.out.println();
    }

    @Override
    public void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Yuriy Tarasov");
        System.out.println("ytarasov@t1-consulting.ru");
        System.out.println();
    }

    @Override
    public void displayArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
        System.out.println();
    }

    @Override
    public void displayCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
        System.out.println();
    }

    @Override
    public void displayError() {
        System.out.println("Error. Type help to show availible commands");
        System.out.println();
    }

    @Override
    public void displayWelcome() {
        System.out.println("** WELCOME TO THE TASK MANAGER **");
    }

    @Override
    public void displayInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = FormatUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
        System.out.println();
    }

}
